#include"Customer.h"
#include<map>

#define NUM_OF_ITEMS 10

void PrintPaysTheMost(map<string, Customer> customerLst);
void AddCustomer(map<string, Customer>& customerLst);
void UpdateCustomer(map<string, Customer>& customerLst);
void AddItems(Item* lst, Customer& c);
void RemoveItems(Customer& c);

Item itemList[NUM_OF_ITEMS] = {
	Item("Milk","00001",5.3),
	Item("Cookies","00002",12.6),
	Item("bread","00003",8.9),
	Item("chocolate","00004",7.0),
	Item("cheese","00005",15.3),
	Item("rice","00006",6.2),
	Item("fish", "00008", 31.65),
	Item("chicken","00007",25.99),
	Item("cucumber","00009",1.21),
	Item("tomato","00010",2.32) };

int main()
{
	map<string, Customer> abcCustomers;
	int choice = 0;

	while (choice != 4)
	{
		cout << "Welcome to MagshiMart! \n" << endl;
		cout << "1. Sign as a costumer and buy items" << endl;
		cout << "2. Update existing customer's items" << endl;
		cout << "3. Print the costumer who pays the most" << endl;
		cout << "4. Exit" << endl;
		cin >> choice;

		switch (choice)
		{
		case 1:
			AddCustomer(abcCustomers);
			break;
		case 2:
			UpdateCustomer(abcCustomers);
			break;
		case 3:
			PrintPaysTheMost(abcCustomers);
			break;
		case 4:
			break;
		default:
			cout << "Invalid Choice. Press any key to continue..." << endl;
			getchar();
			getchar();
			system("cls");
		}
	}
	return 0;
}

void AddItems(Item* lst, Customer& c)
{
	int choice = -1;
	for (int i = 0; i < NUM_OF_ITEMS; i++)
	{
		cout << i + 1 << ". " << lst[i].GetName() << "\t\t" << lst[i].GetUnitPrice() << endl;
	}
	while (choice != 0)
	{
		cout << "Enter Your choice, or 0 to exit: " << endl;
		cin >> choice;
		if (choice != 0)
		{
			if (choice < NUM_OF_ITEMS + 1 && choice > 0)
			{
				c.addItem(lst[choice - 1]);
			}
			else
			{
				cout << "ERROR: index out of range" << endl;
			}
		}
	}
}

void AddCustomer(map<string, Customer>& customerLst)
{
	string name = "";
	cout << "Enter name: ";
	cin >> name;
	getchar();
	if (customerLst.find(name) == customerLst.end())
	{
		Customer c = Customer(name);
		AddItems(itemList, c);
		pair<string, Customer> p = pair<string, Customer>(name, c);
		customerLst.insert(p);
		system("cls");
	}
	else
	{
		cout << "Customer already exists. Press any key to return...";
		getchar();
		system("cls");
	}
}

void UpdateCustomer(map<string, Customer>& customerLst)
{
	string name = "";
	cout << "Enter Name: ";
	cin >> name;
	getchar();

	if (customerLst.find(name) != customerLst.end())
	{
		Customer c = customerLst.find(name)->second;
		system("cls");
		int choice = 0;

		while (choice < 3)
		{
			cout << "1. Add items\n2. Remove Items\n3. Back to menu" << endl;
			cout << "\nEnter your Choice: ";
			cin >> choice;
			getchar();

			switch (choice)
			{
			case 1:
				AddItems(itemList, c);
				break;
			case 2:
				RemoveItems(c);
				break;
			case 3:
				break;
			default:
				cout << "Invalid choice." << endl;
				break;
			}
		}
		system("cls");
	}
	else
	{
		cout << "Customer not found, Press any key to continue..." << endl;
		getchar();
		system("cls");
	}
}

void RemoveItems(Customer & c)
{
	int choice = -1;
	int i = 1;

	while (choice != 0)
	{
		for (auto it : c.GetItems())
		{
			cout << i << "." << it.GetName() << "\t\t" << it.GetUnitPrice() << endl;
			i++;
		}

		cout << "\nEnter your choice or 0 to exit: ";
		cin >> choice;
		getchar();

		if (choice != 0)
		{
			if (choice > 0 && choice < c.GetItems().size())
			{
				i = 0;
				choice = choice - 1;
				for (set<Item>::iterator it = c.GetItems().begin(); it != c.GetItems().end(); it++)
				{
					if (i == choice)
					{
						c.removeItem(*it);
						break;
					}
					i++;
				}
			}
			else
			{
				cout << "Invalid Choice. Press any key..." << endl;
				getchar();
			}
		}
		system("cls");
	}
}

void PrintPaysTheMost(map<string, Customer> customerLst)
{
	auto max = max_element(customerLst.begin(), customerLst.end(),
		[](const pair<string, Customer> p1, const pair <string, Customer> p2) {return p1.second.totalSum() < p2.second.totalSum(); });
	cout << "Name: " << (*max).second.GetName() << "\nItems: " << endl;

	for (auto it : (*max).second.GetItems())
	{
		cout << it.GetName() << "\t\t" << it.GetCount() << endl;
	}

	cout << "Press Any key..." << endl;
	getchar();
	getchar();
	system("cls");
}
