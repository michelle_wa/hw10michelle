#pragma once
#include"Item.h"
#include<set>

class Customer
{
private:
	string _name;
	set<Item> _items;

	bool CheckItemExist(string name);

public:
	Customer(string name);
	~Customer();

	double totalSum() const;//returns the total sum for payment
	void addItem(Item);//add item to the set
	void removeItem(Item);//remove item from the set

	void SetName(string);
	string GetName();
	set<Item>& GetItems();
};
