#pragma once
#include "Customer.h"


Customer::Customer(string name)
{
	this->_name = name;
}

Customer::~Customer() { }

string Customer::GetName()
{
	return this->_name;
}

set<Item>& Customer::GetItems()
{
	return this->_items;
}


void Customer::SetName(string name)
{
	this->_name = name;
}

bool Customer::CheckItemExist(string name)
{
	bool ans = false;
	set<Item>::iterator i;
	for (i = this->_items.begin(); i != this->_items.end(); i++)
	{
		if ((*i).GetName() == name)
		{
			ans = true;
		}
	}
	return ans;
}

double Customer::totalSum() const
{
	double sum = 0;
	for (set<Item>::iterator i = this->_items.begin(); i != this->_items.end(); ++i)
	{
		sum = sum + (*i).totalPrice();
	}
	return sum;
}

void Customer::addItem(Item i)
{
	if (!this->CheckItemExist(i.GetName()))
	{
		this->_items.insert(i);
	}
	else
	{
		for (set<Item>::iterator it = this->_items.begin(); it != this->_items.end(); it++)
		{
			if ((*it).GetName() == i.GetName())
			{
				((Item&)(*it)).SetCount((*it).GetCount() + 1);
			}
		}
	}
}

void Customer::removeItem(Item i)
{
	for (set<Item>::iterator it = this->_items.begin(); it != this->_items.end(); it++)
	{
		if ((*it).GetName() == i.GetName())
		{
			if ((*it).GetCount() > 1)
			{
				((Item&)(*it)).SetCount((*it).GetCount() - 1);
			}
			else
			{
				this->_items.erase(i);
			}
			it = this->_items.end();
		}
	}
}
