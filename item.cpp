#pragma once
#include "Item.h"

using namespace std;

Item::Item(string name, string serialNumber, double unitPrice)
{
	SetName(name);
	SetSerialNumber(serialNumber);
	SetUnitPrice(unitPrice);
	SetCount(1);
}

Item::~Item() { }

double Item::GetUnitPrice() const
{
	return this->_unitPrice;
}

string Item::GetName() const
{
	return this->_name;
}

string Item::GetSerialNumber() const
{
	return this->_serialNumber;
}

int Item::GetCount() const
{
	return this->_count;
}


void Item::SetUnitPrice(double unitPrice)
{
	this->_unitPrice = unitPrice;
}

void Item::SetName(string name)
{
	this->_name = name;
}

void Item::SetSerialNumber(string serialNumber)
{
	this->_serialNumber = serialNumber;
}

void Item::SetCount(int count)
{
	this->_count = count;
}

double Item::totalPrice() const
{
	return this->_count * this->_unitPrice;
}

bool Item::operator<(const Item & other) const
{
	return this->_serialNumber < other._serialNumber;
}

bool Item::operator>(const Item & other) const
{
	return this->_serialNumber > other._serialNumber;
}

bool Item::operator==(const Item & other) const
{
	return this->_serialNumber == other._serialNumber;
}
